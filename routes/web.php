<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::user()){
        if(Auth::user()->role_id == 1){
            return redirect('personal-info');
        }else{
            return redirect('candidates');
        }
    }else{
        return redirect('dashboard');
    }
});

Route::group(['middleware' => ['auth']], function () {
    Route::resources([
        'personal-info' => 'PersonalInfoController',
        'personal-education' => 'EducationController'
    ]);
    Route::get('/candidates', 'PersonalInfoController@candidates')->name('test');
    Route::get('/employees', 'PersonalInfoController@employees')->name('employees');
});

Route::get('/dashboard', function(){
    return view('user-dashboard');
})->name('dashboard');
Route::get('/approve/{id}', 'PersonalInfoController@approve');
Route::get('/reject/{id}', 'PersonalInfoController@reject');
Route::get('/announcement', 'PersonalInfoController@announcement')->name('announcement');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');