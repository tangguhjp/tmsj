<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Education;
use App\PersonalInfo;
use App\Positions;
use App\Skills;
use App\Study;
use App\UserSkill;

use Auth;
use Session;
use StdClass;

class EducationController extends Controller
{

    public function __construct(){
        $this->education = new Education();
        $this->skills = new Skills();
        $this->study = new Study();
        $this->personal = new PersonalInfo();
        $this->positions = new Positions();
        $this->user_skill = new UserSkill();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal = $this->personal->getData();
        if(!$personal){
            Session::flash('alert-warning', 'Please input personal data!');
            return redirect()->route('personal-info.index');
        }
        $datas = $this->initEducationComponent();
        $education = $this->education->getData($this->personal->getData()->id);
        return view('personal-education', compact(
            'datas', 'education', 'personal'
        ));
    }

    public function initEducationComponent(){
        $data = new StdClass();
        $data->study = $this->study->getAll();
        $data->positions = $this->positions->getAll();
        $data->skills = $this->skills->getAll();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Education();
        $data->personal_info_id = $this->personal->getData()->id;
        $data->study_id = $request->study;
        $data->position_id = $request->position;
        $data->gpa = $request->gpa;
        if($data->save()){
            $this->storeSkills($data->personal_info_id, $request->skills);
            Session::flash('alert-success','Education data updated!');
            return back();
        }
    }

    public function storeSkills($personal_id, $skills){
        if(count($skills) > 0){
            $collectData = [];
            foreach($skills as $skill){
                $data['personal_info_id'] = $personal_id;
                $data['skill_id'] = $skill;
                array_push($collectData, $data);
            }
            UserSkill::insert($collectData);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
