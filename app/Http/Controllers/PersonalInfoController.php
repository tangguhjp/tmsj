<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gender;
use App\PersonalInfo;
use Auth;
use Session;
use StdClass;

class PersonalInfoController extends Controller
{

    public function __construct(){
        $this->personal = new PersonalInfo();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personalData = PersonalInfo::where('user_id', Auth::id())->first();
        $gender = Gender::all();
        return view('personal-data-form', compact('personalData', 'gender'));
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new PersonalInfo();
        $data->user_id = Auth::id();
        $data->status_id = 1;
        $data->gender_id = $request->gender;
        $data->firstname = $request->firstname;
        $data->lastname = $request->lastname;
        $data->birthday = strtotime($request->birthday);
        $data->phone = $request->phone;
        $data->address = $request->address;

        if($data->save()){
            Session::flash('alert-success','Personal data updated!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PersonalInfo::find($id);
        $data->gender;
        $data->education->position;
        $data->education->study;
        $data->birthday = date('d M Y', $data->birthday);

        $skill = [];
        foreach($data->skills as $s){
            $skill[] = $s->skill->description;
        }
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function candidates(){
        $candidates = $this->personalValidate($this->personal->candidates());
        return view('candidates', compact('candidates'));
    }

    public function approve($id){
        $data = PersonalInfo::find($id);
        $data->status_id = 2;
        if($data->save()){
            return back();
        }
    }
    public function reject($id){
        $data = PersonalInfo::find($id);
        $data->status_id = 3;
        if($data->save()){
            return back();
        }
    }

    public function employees(){
        $employees = $this->personalValidate($this->personal->employees());
        return view('employees', compact('employees'));
    }

    public function announcement(){
        $personals = $this->personalValidate(PersonalInfo::all());
        $dataTotal = new StdClass();
        $dataTotal->pending = $this->personal->countByStatus(1);
        $dataTotal->approved = $this->personal->countByStatus(2);
        $dataTotal->rejected = $this->personal->countByStatus(3);
        return view('announcement', compact('personals', 'dataTotal'));
    }


    public function personalValidate($personals){
        $result = [];
        foreach($personals as $p){
            if($p->education){
                array_push($result, $p);
            }
        }
        return $result;
    }
}
