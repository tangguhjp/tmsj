<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $table = 'study';

    public function getAll(){
        return self::orderBy('description')->get();
    }
}
