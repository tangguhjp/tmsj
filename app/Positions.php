<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    protected $table = 'positions';

    public function getAll(){
        return self::orderBy('description')->get();
    }
}
