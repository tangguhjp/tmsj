<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'education';
    public $timestamps = false;

    public function study()
    {
        return $this->belongsTo('App\Study');
    }

    public function position()
    {
        return $this->belongsTo('App\Positions');
    }

    public function getData($personal_id){
        $data = self::where('personal_info_id', $personal_id)->first();
        if($data){
            return $data;
        }
        return false;
    }
}
