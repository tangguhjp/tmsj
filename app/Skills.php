<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model
{
    protected $table = 'skills';

    public function getAll(){
        return self::orderBy('description')->get();
    }
}
