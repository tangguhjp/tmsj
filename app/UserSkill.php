<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model
{
    protected $table = 'user_skill';
    public $timestamps = false;

    protected $fillable = [
        'personal_info_id', 'skill_id'
    ];

    public function skill()
    {
        return $this->belongsTo('App\Skills');
    }
    public function getData($personal_id){
        $data = self::where('personal_info_id', $personal_id)->first();
        if($data){
            return $data;
        }
        return false;
    }
}
