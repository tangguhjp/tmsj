<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PersonalInfo extends Model
{
    protected $table = 'personal_info';
    public $timestamps = false;    

    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function skills()
    {
        return $this->hasMany('App\UserSkill');
    }

    public function education()
    {
        return $this->hasOne('App\Education');
    }
    public function getData(){
        $data = self::where('user_id', Auth::id())->first();
        if($data){
            return $data;
        }
        return false;
    }

    public function employees(){
        $data = self::where('status_id', 2)->get();
        if($data){
            return $data;
        }
        return false;
    }

    public function candidates(){
        $data = self::where('status_id', 1)->get();
        if($data){
            return $data;
        }
        return false;
    }

    public function countByStatus($status){
        return count(self::where('status_id', $status)->get());
    }
}
