<div class="modal fade detail-personal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Personal Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-12 card">
                                <div class="card-body">
                                    <h5 class="card-title">Personal Data</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="active list-group-item-action list-group-item">Full Name</p>
                                            <p class="list-group-item-action list-group-item fullname"></p>
                                            <p class="active list-group-item-action list-group-item">Gender</p>
                                            <p class="list-group-item-action list-group-item gender"></p>
                                            <p class="active list-group-item-action list-group-item">Date of Birthday</p>
                                            <p class="list-group-item-action list-group-item birthday"></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="active list-group-item-action list-group-item">Phone</p>
                                            <p class="list-group-item-action list-group-item phone"></p>
                                            <p class="active list-group-item-action list-group-item">Address</p>
                                            <p class="list-group-item-action list-group-item address"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="col-md-12">
                            <div class="main-card mb-6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Education Data</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="active list-group-item-action list-group-item">Study Program</p>
                                            <p class="list-group-item-action list-group-item study-program"></p>
                                            <p class="active list-group-item-action list-group-item">Grade Point Average (GPA)</p>
                                            <p class="list-group-item-action list-group-item gpa"></p>
                                            <p class="active list-group-item-action list-group-item">Desired Position</p>
                                            <p class="list-group-item-action list-group-item position"></p>
                                        </div>
                                        <div class="col-md-6 expertise">
                                            <p class="active list-group-item-action list-group-item">Certification of Expertise</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger set-id reject" >Reject</button>
                    <button type="button" class="btn btn-success set-id approve" >Approve</button>
                </div>
            </div>
        </div>
    </div>