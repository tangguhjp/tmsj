@extends('main', ['currentMenu' => 'mn-personal-data'])
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Personal Data
                @if($personalData)
                    <div class="page-title-subheading">
                        Your personal data bellow
                    </div>
                @else
                    <div class="page-title-subheading">
                        Please input all required data
                    </div>
                @endif
            </div>
        </div> 
</div>
</div>            
@if($personalData)
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Personal data</h5>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">First Name</h6>
                    <p>{{$personalData->firstname}}</p>
                </div>
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Last Name</h6>
                    <p>{{$personalData->lastname}}</p>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Gender</h6>
                    <p>{{$personalData->gender->description}}</p>
                </div>
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Date of Birth</h6>
                    <p>{{ date('d M Y' , $personalData->birthday)}}</p>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Phone Number</h6>
                    <p>{{$personalData->phone}}</p>
                </div>
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Address</h6>
                    <p>{{$personalData->address}}</p>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Personal data form</h5>
            <form action="{{route('personal-info.store')}}" method="post" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-firstname">First name</label>
                        <input name="firstname" type="text" class="form-control" id="input-firstname" placeholder="First name" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-lastname">Last name</label>
                        <input name="lastname" type="text" class="form-control" id="input-lastname" placeholder="Last name" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="select-gender">Gender</label>
                        <select name="gender" id="select-gender" class="form-control">
                            @foreach($gender as $gen)
                                <option value="{{$gen->id}}">{{$gen->description}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-birthday">Date of Birth</label>
                        <input name="birthday" type="date" class="form-control" id="input-birthday" placeholder="Last name" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-phone">Phone Number</label>
                        <input name="phone" type="text" class="form-control" id="input-phone" placeholder="+6282xxxxxx" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-address">Address</label>
                        <input name="address" type="text" class="form-control" id="input-address" placeholder="Ir. Soekarno Street, No. 23" required>
                        <div class="invalid-feedback">
                            Please provide a valid address.
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label" for="invalidCheck">
                            I have correctly entered the data. 
                        </label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>

            <script>
                // Example starter JavaScript for disabling form submissions if there are invalid fields
                (function() {
                    'use strict';
                    window.addEventListener('load', function() {
                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                        var forms = document.getElementsByClassName('needs-validation');
                        // Loop over them and prevent submission
                        var validation = Array.prototype.filter.call(forms, function(form) {
                            form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                            }, false);
                        });
                    }, false);
                })();
            </script>
        </div>
    </div>
@endif
@endsection