@if(Session::has('alert-success'))
    <div id="alert-session">
        <div class="alert alert-success fade show" role="alert">
            <p class="mb-0">{{Session::get('alert-success')}}</p>
        </div>
    </div>
@endif
@if(Session::has('alert-warning'))
    <div id="alert-session">
        <div class="alert alert-warning fade show" role="alert">
            <p class="mb-0">{{Session::get('alert-warning')}}</p>
        </div>
    </div>
@endif  