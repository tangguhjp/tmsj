@extends('main', ['currentMenu' => 'mn-announcement'])
@section('content')
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Announcement</div>
                </div>   
            </div>
        </div>            
        <div class="row">
            <div class="col-md-4 col-xl-4">
                <div class="card mb-3 widget-content badge-warning">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Pending</div>
                            <div class="widget-subheading">Hope for the best and prepare for the worst.</div>
                            <br>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$dataTotal->pending}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xl-4">
                <div class="card mb-3 widget-content bg-grow-early">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Approved</div>
                            <div class="widget-subheading">Don’t be arrogant, because arrogant kills curiosity and passion.</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$dataTotal->approved}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xl-4">
                <div class="card mb-3 widget-content badge-danger">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Rejected</div>
                            <div class="widget-subheading">Remember one thing, the work we do will be worth it.</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$dataTotal->rejected}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Candidates</div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Status</th>
                                @if(Auth::user() && Auth::user()->role_id == 2)
                                    <th class="text-center">Actions</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                                @if($personals)
                                    @foreach($personals as $p)
                                    <tr>
                                        <td class="text-center text-muted">
                                            CAN-{{$p->id}}
                                        </td>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">
                                                            {{$p->firstname}} {{$p->lastname}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">{{$p->education->position->description}}</td>
                                        <td class="text-center">
                                            @if($p->status_id == 1)
                                                <div class="badge badge-warning">Pending</div>
                                            @elseif($p->status_id == 2)
                                                <div class="badge badge-success">Approved</div>
                                            @else
                                                <div class="badge badge-danger">Rejected</div>
                                            @endif
                                        </td>
                                        @if(Auth::user() && Auth::user()->role_id == 2)
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary btn-sm btn-detail-personal" data-personal-id="{{$p->id}}" 
                                            data-toggle="modal" data-target=".detail-personal">Details</button>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection