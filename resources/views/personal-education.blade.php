@extends('main', ['currentMenu' => 'mn-education'])
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Education
                @if($personal)
                    <div class="page-title-subheading">
                        Your education data bellow
                    </div>
                @else
                    <div class="page-title-subheading">
                        Please input all required data
                    </div>
                @endif
            </div>
        </div> 
</div>
</div>    
@if($education)
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Education Data</h5>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Study Program</h6>
                    <p>{{$education->study->description}}</p>
                </div>
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Grade Point Average (GPA)</h6>
                    <p>{{$education->gpa}}</p>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Certification of Expertise</h6>
                    <div id="skills">
                        @foreach($personal->skills as $sk)
                            <div class="custom-checkbox custom-control">
                                <li>{{$sk->skill->description}}</li>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <h6 class="badge badge-primary">Desired Position</h6>
                    <p>{{$education->position->description}}</p>
                </div>
            </div>
        </div>
    </div>
@else        
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Personal Skills form</h5>
            <form action="{{route('personal-education.store')}}" method="post" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="select-study badge-warning">Study Program</label>
                        <select name="study" id="select-study" class="form-control">
                            @foreach($datas->study as $std)
                                <option value="{{$std->id}}">{{$std->description}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-gpa">Grade Point Average (GPA)</label>
                        <input name="gpa" type="number" class="form-control" id="input-gpa" max="4" step="0.01" placeholder="Scale 1-4" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label class="badge badge-primary" for="skills">Certification of Expertise</label>
                        <div id="skills">
                            @foreach($datas->skills as $sk)
                                <div class="custom-checkbox custom-control">
                                    <input name="skills[]" type="checkbox" id="skill-{{$sk->id}}" class="custom-control-input" value="{{$sk->id}}">
                                    <label class="custom-control-label" for="skill-{{$sk->id}}">
                                        {{$sk->description}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label class="badge badge-primary" for="input-position">Desired Position</label>
                        <select name="position" id="input-position" class="form-control">
                            @foreach($datas->positions as $pos)
                                <option value="{{$pos->id}}">{{$pos->description}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>

            <script>
                // Example starter JavaScript for disabling form submissions if there are invalid fields
                (function() {
                    'use strict';
                    window.addEventListener('load', function() {
                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                        var forms = document.getElementsByClassName('needs-validation');
                        // Loop over them and prevent submission
                        var validation = Array.prototype.filter.call(forms, function(form) {
                            form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                            }, false);
                        });
                    }, false);
                })();
            </script>
        </div>
    </div>
@endif
@endsection