@extends('main', ['currentMenu' => 'mn-home'])
@section('content')
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Welcome to PT. PRAS !!!</div>
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="mb-12 card card-body">
                    <h5 class="card-title">
                        JOIN WITH US.
                    </h5>
                        With the main business in the development of software applications 
                        and ICT service providers. The company's main activity is 
                        software development for application product research or project completion.
                    <!-- <button class="btn btn-primary"> -->
                        <a class="btn btn-primary" href="{{route('login')}}">Apply now!</a>
                    <!-- </button> -->
                </div>
            </div>
        </div>
@endsection