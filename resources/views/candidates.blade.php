@extends('main', ['currentMenu' => 'mn-candidates'])
@section('content')
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Data candidates</div>
                </div>   
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Candidates</div>
                            <div class="widget-subheading">People who have dreams of working in your company.</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{count($candidates)}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Candidates</div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Study Program</th>
                                <th class="text-center">Desired Position</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($candidates)
                                    @foreach($candidates as $can)
                                    <tr>
                                        <td class="text-center text-muted">{{$can->id}}</td>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">{{$can->firstname}} {{$can->lastname}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">{{$can->education->study->description}}</td>
                                        <td class="text-center">{{$can->education->position->description}}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary btn-sm btn-detail-personal" data-personal-id="{{$can->id}}" 
                                            data-toggle="modal" data-target=".detail-personal">Details</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection