<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Recruitment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="{{ asset('main') }}/main.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />

</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <!-- <div class="logo-src"></div> -->
                <h5>PT. PRAS</h5>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        @if(Auth::user())
                                            {{Auth::user()->name}}
                                        @else
                                            {{ __('Login / Register') }}
                                        @endif
                                    </div>
                                </div>
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            @if(Auth::user())
                                                <a ref="{{ route('logout') }}" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();" tabindex="0" class="dropdown-item">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @else
                                                <a href="{{route('login')}}" tabindex="0" class="dropdown-item">Login</a>
                                                <a href="{{route('register')}}" tabindex="0" class="dropdown-item">Register</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>        
           
        <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Dashboards</li>
                                @if(Auth::user())
                                    @if(Auth::user()->role_id == 1)
                                    <li>
                                        <a id="mn-personal-data" href="{{route('personal-info.index')}}">
                                            <i class="metismenu-icon pe-7s-user"></i>
                                            Personal Data
                                        </a>
                                    </li>
                                    <li>
                                        <a id="mn-education" href="{{route('personal-education.index')}}">
                                            <i class="metismenu-icon pe-7s-study"></i>
                                            Education
                                        </a>
                                    </li>
                                    @endif
                                    @if(Auth::user()->role_id == 2)
                                    <li>
                                        <a id="mn-candidates" href="{{route('test')}}">
                                            <i class="metismenu-icon pe-7s-users"></i>
                                            Candidates
                                        </a>
                                    </li>
                                    <li>
                                        <a id="mn-employees" href="{{route('employees')}}">
                                            <i class="metismenu-icon pe-7s-id"></i>
                                            Employees
                                        </a>
                                    </li>
                                    @endif
                                @else
                                    <li>
                                        <a id="mn-home" href="{{route('dashboard')}}">
                                            <i class="metismenu-icon pe-7s-home"></i>
                                            Home
                                        </a>
                                    </li>
                                @endif
                                
                                <li class="app-sidebar__heading">Other</li>
                                <li>
                                    <a id="mn-announcement" href="{{route('announcement')}}">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        Announcement
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <div class="app-main__outer">
                    <div class="app-main__inner">
                    @include('flash-message')
                    <br>
                    @yield('content')
                    </div>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                                <div class="app-footer-right">
                                    <ul class="nav">
                                        <li class="nav-item">
                                            TMSJ 2019
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
        </div>
    </div>
    @include('modals')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script type="text/javascript" src="{{ asset('main') }}/scripts/main.js"></script>
    <script>
        currentMenu = "<?php echo $currentMenu ?>";
        $(document).ready(function(){
            $("a#"+currentMenu).addClass("mm-active");
            alertSession = $("#alert-session");
            if(alertSession.length != 0){
                alertSession.fadeTo(2000, 500).slideUp(500, function(){
                    alertSession.slideUp(300);
                });
            }

            $('.btn-detail-personal').click(function(){
                personalId = $(this).data('personal-id');
                $.get("/personal-info/"+personalId, function( data ) {
                    $('.set-id').css("display", "none");

                    $('.detail-personal .fullname').text(data.firstname+" "+data.lastname);
                    $('.detail-personal .gender').text(data.gender.description);
                    $('.detail-personal .birthday').text(data.birthday);
                    $('.detail-personal .phone').text(data.phone);
                    $('.detail-personal .address').text(data.address);
                    $('.detail-personal .study-program').text(data.education.study.description);
                    $('.detail-personal .gpa').text(data.education.gpa);
                    $('.detail-personal .position').text(data.education.position.description);
                    $.each(data.skills, function( index, value ) {
                        $('.expertise').append( "<p class='list-group-item-action list-group-item'>"+
                        value.skill.description
                        +"</p>" );
                    });
                    
                    if(currentMenu == "mn-candidates"){
                        $('.set-id').css("display", "block");
                        $('.set-id').data("personal-id", data.id);
                    }
                });
            })

            $('.approve').click(function(){
                if(confirm("Do you seriously want to approve these candidates?")) 
                    document.location = "/approve/"+$(this).data('personal-id');
            })

            $('.reject').click(function(){
                if(confirm("Do you seriously want to reject these candidates?")) 
                    document.location = "/reject/"+$(this).data('personal-id');
            })
        })
    </script>
</body>
</html>
