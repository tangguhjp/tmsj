<?php

use Illuminate\Database\Seeder;

class StudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('study')->insert([
            [
                'description' => 'Computer Engineering'
            ],
            [
                'description' => 'Informatics'
            ],
            [
                'description' => 'Information System'
            ],
        ]);
    }
}
