<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([
            [
                'description' => 'Web Programming'
            ],
            [
                'description' => 'Mobile Programming'
            ],
            [
                'description' => 'Database Administration'
            ],
        ]);
    }
}
