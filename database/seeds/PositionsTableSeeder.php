<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'description' => 'Programmer'
            ],
            [
                'description' => 'Data Analyst'
            ],
            [
                'description' => 'Text Writer'
            ],
        ]);
    }
}
